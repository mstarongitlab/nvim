require "nvchad.mappings"

-- add yours here

local map = vim.keymap.set

map("n", ";", ":", { desc = "CMD enter command mode" })
map("i", "jk", "<ESC>")

-- Go stuff
map("n", "<leader>db", "<cmd> DapToggleBreakpoint <CR>", { desc = "Go Add a breakpoint at line" })
map("n", "<leader>dus", function()
  local widgets = require "dap.ui.widgets"
  local sidebar = widgets.sidebar(widgets.scopes)
  sidebar.open()
end, { desc = "Go Open debugging sidebar" })
map("n", "<leader>dgt", function()
  require("dap-go").debug_test()
end, { desc = "Go Debug go test" })
map("n", "<leader>dgl", function()
  require("dap-go").debug_last()
end, { desc = "Go Debug last test" })
map("n", "<leader>gsj", "<cmd> GoTagAdd json <CR>", { desc = "Go Add json struct tags" })
map("n", "<leader>gsy", "<cmd> GoTagAdd yaml <CR>", { desc = "Go Add yaml struct tags" })

-- handy shortcut
map({ "n", "i", "v" }, "<C-s>", "<cmd> w <cr>", { desc = "Nvim Save file" })

-- telescope
map({ "n" }, "<leader>fda", require("telescope.builtin").diagnostics, { desc = "Telescope List all diagnostics" })
map({ "n" }, "<leader>fds", function()
  require("telescope.builtin").diagnostics { bufnr = 0 }
end, { desc = "Telescope List diagnostics in current buffer" })
map({ "n" }, "<leader>fqf", require("telescope.builtin").quickfix, { desc = "Telescope Show quickfix list" })
map(
  { "n" },
  "<leader>fr",
  require("telescope.builtin").lsp_references,
  { desc = "Telescope Show references to hovered symbol" }
)
