Cutil = {}

-- check_and_add checks if a given binary name exists and is runnable
-- If it does, it adds the given names to the target array, then returns the modified version
Cutil.check_and_add = function(required_binary, names, arr)
  if vim.fn.executable(required_binary) == 1 then
    for _, i in ipairs(names) do
      table.insert(arr, i)
    end
  end
  return arr
end

-- Wrapper for check_and_add that handles array boilerplate
Cutil.chain_check = function(pairs)
  local res = {}
  for _, pair in ipairs(pairs) do
    res = Cutil.check_and_add(pair[1], pair[2], res)
  end
  return res
end

-- Ensures that a given array will always contain a set of values
Cutil.always_include = function(vals, into)
  for _, val in ipairs(vals) do
    if Cutil.has_value(into, val) then
      goto continue
    end
    table.insert(into, val)
    ::continue::
  end
  return into
end

-- Check if an array contains a value
Cutil.has_value = function(list, val)
  for _, check in ipairs(list) do
    if val == check then
      return true
    end
  end
  return false
end
