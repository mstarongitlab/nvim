require "custom_util"
local w = {
  {
    "stevearc/conform.nvim",
    event = { "BufWritePre", "BufNewFile" },
    config = function()
      require "configs.conform"
    end,
  },

  {
    "nvim-treesitter/nvim-treesitter",
    opts = {
      ensure_installed = Cutil.always_include(
        { "vim", "lua", "vimdoc" },
        Cutil.chain_check {
          { "go",     { "go" } },
          {
            "node",
            {
              "glimmer",
              "javascript",
              "typescript",
              "html",
              "css",
            },
          },
          { "cargo",  { "rust" } },
          { "elixir", { "elixir", "eex", "heex" } },
        }
      ),
    },
  },

  {
    "nvimtools/none-ls.nvim",
    ft = "go",
    opts = function()
      return require "configs.null-ls"
    end,
  },

  {
    "mfussenegger/nvim-dap",
    config = function()
      require "configs.dap"
    end,
    keys = {
      "<leader>db",
      "<cmd> DapToggleBreakpoint <CR>",
      desc = "Dap Toggle breakpoint",
    },
  },

  {
    "nvim-tree/nvim-tree.lua",
    version = "*",
    lazy = false,
    dependencies = "nvim-tree/nvim-web-devicons",
    config = function()
      require("nvim-tree").setup {}
    end,
  },

  {
    "joukevandermaas/vim-ember-hbs",
    ft = { "ember", "hbs", "html/handlebars" },
  },

  {
    "rcarriga/nvim-dap-ui",
    event = "VeryLazy",
    dependencies = {
      "mfussenegger/nvim-dap",
      "nvim-neotest/nvim-nio",
    },
    config = function()
      local dap = require "dap"
      local dapui = require "dapui"
      require("dapui").setup()
      dap.listeners.after.event_inisialized["dapui_config"] = function()
        dapui.open()
      end
      dap.listeners.before.event_terminated["dapui_config"] = function()
        dapui.close()
      end
      dap.listeners.before.event_exited["dapui_config"] = function()
        dapui.close()
      end
    end,
    keys = {
      {
        "<leader>dus",
        function()
          local widgets = require "dap.ui.widgets"
          local sidebar = widgets.sidebar.open(widgets.scopes)
          sidebar.open()
        end,
        desc = "Dap Open debugging sidebar",
      },
    },
  },

  {
    "folke/neodev.nvim",
    opts = {},
  },
}

return w
