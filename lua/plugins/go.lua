return {
  {
    "olexsmir/gopher.nvim",
    ft = "go",
    config = function(_, opts)
      require("gopher").setup(opts)
    end,
    build = function()
      vim.cmd [[silent! GoInstallDeps]]
    end,
    keys = {
      { "<leader>gsj", "<cmd> GoTagAdd json <CR>", desc = "Go Add json struct tags" },
      { "<leader>gsy", "<cmd> GoTagAdd yaml <CR>", desc = "Go Add yaml struct tags" },
      { "<leader>gst", "<cmd> GoTagAdd toml <CR>", desc = "Go Add toml struct tags" },
      { "<leader>gsg", "<cmd> GoTagAdd gorm <CR>", desc = "Go Add gorm struct tags" },
    },
  },
  {
    "leoluz/nvim-dap-go",
    ft = "go",
    dependencies = "mfussenegger/nvim-dap",
    config = function(_, opts)
      require("dap-go").setup(opts)
    end,
    keys = {
      {
        "<leader>dgt",
        function()
          require("dap-go").debug_test()
        end,
        desc = "Go Debug Go test",
      },
    },
  },
}
