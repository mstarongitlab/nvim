require "custom_util"
local m = {
  {
    "williamboman/mason.nvim",
    config = require("mason").setup,
  },

  {
    "williamboman/mason-lspconfig.nvim",
    config = function()
      require("mason-lspconfig").setup {
        ensure_installed = Cutil.always_include(
          { "lua_ls" },
          Cutil.chain_check {
            { "go", { "gopls" } },
            { "cargo", { "rust_analyzer" } },
            {
              "node",
              {
                "angularls",
                "astro",
                "cssls",
                "eslint",
                "ember",
                "html",
                "biome", -- ts, js and json
                "svelte",
              },
            },
            { "docker", { "dockerls", "docker_compose_language_service" } },
            { "python", { "pyright" } },
            -- { "godot", { "gdtoolkit" } },
            { "elixir", { "elixirls" } },
          }
        ),
      }
    end,
    dependencies = "williamboman/mason.nvim",
  },

  {
    "neovim/nvim-lspconfig",
    config = function()
      require("nvchad.configs.lspconfig").defaults()
      require "configs.lspconfig"
    end,
    dependencies = "williamboman/mason-lspconfig.nvim",
  },
}
return m
