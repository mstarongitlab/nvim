local null_ls = require "null-ls"
local augroup = vim.api.nvim_create_augroup("LspFormatting", {})

return {
  sources = {
    -- Go
    null_ls.builtins.diagnostics.golangci_lint,
    null_ls.builtins.code_actions.gomodifytags,
    null_ls.builtins.formatting.gofmt,
    null_ls.builtins.formatting.goimports_reviser,
    null_ls.builtins.formatting.golines,
    -- Lua
    null_ls.builtins.formatting.stylua,
    -- JS/TS
    null_ls.builtins.formatting.prettier,
    -- Text thing
    -- null_ls.builtins.completion.spell,
    -- .env
    null_ls.builtins.diagnostics.dotenv_linter,
    -- Gdscript
    null_ls.builtins.diagnostics.gdlint,
    -- Dockerfile
    null_ls.builtins.diagnostics.hadolint,
    -- Markdown
    null_ls.builtins.diagnostics.markdownlint,
    -- ProtoBuf
    null_ls.builtins.diagnostics.protolint,
    -- Python
    null_ls.builtins.diagnostics.pylint.with {
      diagnostics_postprocess = function(diagnostic)
        diagnostic.code = diagnostic.message_id
      end,
    },
    null_ls.builtins.formatting.isort,
    null_ls.builtins.formatting.black,
    -- Json/Yaml
    null_ls.builtins.diagnostics.spectral,
    -- TODOs
    null_ls.builtins.diagnostics.todo_comments,
  },
  on_attach = function(client, bufnr)
    if client.supports_method "textDocument/formatting" then
      vim.api.nvim_clear_autocmds {
        group = augroup,
        buffer = bufnr,
      }
      vim.api.nvim_create_autocmd("BufWritePre", {
        group = augroup,
        buffer = bufnr,
        callback = function()
          vim.lsp.buf.format { bufnr = bufnr }
        end,
      })
    end
  end,
}
