local options = {
  -- formatters_by_ft = {
  --   css = { "biome" },
  --   html = { "biome" },
  --   javascript = { "biome" },
  --   typescript = { "biome" },
  --   javascriptreact = { "biome" },
  --   typescriptreact = { "biome" },
  --   svelte = { "biome" },
  --   json = { "biome" },
  --   yaml = { "biome" },
  --   markdown = { "biome" },
  --   graphql = { "biome" },
  --   lua = { "stylua" },
  --   go = { "gofmt" },
  --   python = { "isort", "black" },
  -- },
  --
  formatters_by_ft = {
    css = { "prettier" },
    html = { "prettier" },
    javascript = { "prettier" },
    typescript = { "prettier" },
    javascriptreact = { "prettier" },
    typescriptreact = { "prettier" },
    svelte = { "prettier" },
    json = { "prettier" },
    yaml = { "prettier" },
    markdown = { "prettier" },
    graphql = { "prettier" },
    lua = { "stylua" },
    go = { "gofmt" },
    python = { "isort", "black" },
  },

  format_on_save = {
    -- These options will be passed to conform.format()
    timeout_ms = 500,
    lsp_fallback = true,
  },
}

vim.keymap.set({ "n", "v" }, "<leader>cf", function()
  require("conform").format {
    timeout_ms = 500,
    async = false,
    lsp_fallback = true,
  }
end, { desc = "Conform: Format file or selection" })
require("conform").setup(options)
