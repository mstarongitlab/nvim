return {
  enabled = true,
  cloak_character = "*",
  highlight_group = "Comment",
  -- Make everything look like it's length 12
  cloack_length = 12,
  try_all_patterns = true,
  cloack_telescope = true,
  patterns = {
    -- .env files
    {
      file_pattern = ".env",
      cloack_pattern = { "=.+", "= .+" },
      replace = nil,
    },
    {
      file_pattern = ".toml",
      cloack_pattern = {
        "(.+\\_token = ).+",
      },
    },
  },
}
